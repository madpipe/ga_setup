# GeekAliens .bashrc file
# Author of original file: Balaji S. Srinivasan (balajis@stanford.edu)
# Current author: Ovidiu-Florin Bogdan (GeekAliens.com)
#
# See the Wiki on GitHub for more info.


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Setting Umask Permissions

if [ "`id -gn`" == "`id -un`" -a `id -u` -gt 99 ]; then
	umask 002
else
	umask 022
fi


# Make prompt informative
# See:  http://www.ukuug.org/events/linux2003/papers/bash_tips/
#PS1="\[\033[0;34m\][\u@\h:\w]$\[\033[0m\]"
PS1="[\[\033[01;33m\]\u\[\033[00m\]\[\033[01;31m\]@\[\033[00m\]\[\033[01;32m\]\h \[\033[01;34m\]\W\[\033[00m\]]\[\033[01;32m\]\$\[\033[00m\] "

## -----------------------
## -- 2) Set up aliases --
## -----------------------

# support for using these aliases with sudo
alias sudo='sudo '

# 2.1) Safety
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -ir"
#set -o noclobber

# 2.2) Listing, directories, and motion
alias ls='ls --color'
alias l='ls -lah'
alias m='mcedit'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../../'
alias cl='clear'
alias du='du -ch --max-depth=1'
alias treeacl='tree -A -C -L 2'

# 2.3) Text and editor commands
alias em='emacs -nw'     # No X11 windows
alias emq='emacs -nw -Q' # No config and no X11
export EDITOR='emacs -nw'
export VISUAL='emacs -nw' 

# 2.4) grep options
export GREP_OPTIONS='--color=auto'
export GREP_COLOR='1;31' # green for matches

# 2.5) sort options
# Ensures cross-platform sorting behavior of GNU sort.
# http://www.gnu.org/software/coreutils/faq/coreutils-faq.html#Sort-does-not-sort-in-normal-order_0021
unset LANG
export LC_ALL=POSIX

if [ -d $HOME/bin ]; then
	export PATH=$PATH:$HOME/bin
fi

# Import custom settings, if the user has any
# Custom settings such as ssh aliases to personal computers or other stuff that due to security concerns cannot be public.
if [ -f $HOME/.bashrc_custom ]; then
	. $HOME/.bashrc_custom
fi

# git completion (especially for Arch Linux)

if [ -f /usr/share/git/completion/git-completion.bash ]; then
	. /usr/share/git/completion/git-completion.bash
fi

