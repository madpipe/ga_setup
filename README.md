ga_setup.git
============

###The GeekAliens Linux Workspace setup

This repo contains the configuration files for Bash, Emacs, GNU Screen and others, that are customised to my personal preferences.

This is a fork for https://github.com/startup-class/dotfiles.
